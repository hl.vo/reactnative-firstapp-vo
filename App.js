import React, { useState } from "react";
import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, Button, TextInput } from "react-native";
import { TouchableOpacity } from "react-native";
import Goal from "./components/Goal";

export default function App() {
  const sampleGoals = [
    "Faire les courses",
    "Aller à la salle de sport 3 fois par semaine",
    "Monter à plus de 5000m d altitude",
    "Acheter mon premier appartement",
    "Perdre 5 kgs",
  ];
  const [goal, setGoal] = useState();
  const [goalItems, setGoalItems] = useState(sampleGoals);

  const handleAddGoal = () => {
    setGoalItems([...goalItems, goal]);
    setGoal(null);
  };

  const completeGoals = (index) => {
    let itemsCopy = [...goalItems];
    itemsCopy.splice(index, 1);
    setGoalItems(itemsCopy);
  };

  return (
    <View style={styles.container}>
      <Text>
        Open up <Text style={{ fontWeight: "bold" }}>App.js</Text> to start
        working on your app!
      </Text>
      <StatusBar style="auto" />

      {/* Les objectifs à faire */}
      <View style={styles.tasksWrapper}>
        <Text style={styles.sectionTitle}>Les objectifs à faire</Text>

        {/* Les tâches */}
        <View style={styles.items}>
          {goalItems.map((goal, index) => {
            return (
              <>
                <Goal key={index} text={goal} />
                <TouchableOpacity onPress={() => completeGoals()}>
                  <Text>X</Text>
                </TouchableOpacity>
              </>
            );
          })}
        </View>
      </View>

      <View style={styles.addWrapper}>
        <TextInput
          style={styles.input}
          placeholder={"Ajouter un objectif"}
          value={goal}
          onChangeText={(text) => setGoal(text)}
        />
        <TouchableOpacity onPress={() => handleAddGoal()}>
          <Text styles={styles.addText}>Add</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "gray",
    color: "red",
    fontSize: 20,
  },

  tasksWrapper: {
    paddingTop: 80,
    paddingHorizontal: 20,
  },

  sectionTitle: {
    fontSize: 24,
    fontWeight: "bold",
  },

  items: {
    marginTop: 30,
    flexDirection: "row",
  },

  input: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: "#FFF",
    borderRadius: 60,
    borderColor: "#C0C0C0",
    borderWidth: 1,
    width: 250,
    marginRight: 5,
  },

  addWrapper: {
    backgroundColor: "#FFF",
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#C0C0C0",
    borderWidth: 1,
    flexDirection: "row",
  },

  deleteInput: {},
});
